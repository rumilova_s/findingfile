﻿namespace AppFindingFile
{
    partial class searchFileForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(searchFileForm));
            this.FindButton = new System.Windows.Forms.Button();
            this.StopButton = new System.Windows.Forms.Button();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.InputPattern = new System.Windows.Forms.TextBox();
            this.InputTextInFile = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InputDirectory = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCheckNowFile = new System.Windows.Forms.Label();
            this.lblcountOfFiles = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.ProcessData = new System.Windows.Forms.GroupBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.InputDatas = new System.Windows.Forms.GroupBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.ProcessData.SuspendLayout();
            this.InputDatas.SuspendLayout();
            this.SuspendLayout();
            // 
            // FindButton
            // 
            this.FindButton.Location = new System.Drawing.Point(12, 12);
            this.FindButton.Name = "FindButton";
            this.FindButton.Size = new System.Drawing.Size(75, 23);
            this.FindButton.TabIndex = 0;
            this.FindButton.Text = "Find";
            this.FindButton.UseVisualStyleBackColor = true;
            this.FindButton.Click += new System.EventHandler(this.FindButton_Click);
            // 
            // StopButton
            // 
            this.StopButton.Enabled = false;
            this.StopButton.Location = new System.Drawing.Point(93, 12);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(75, 23);
            this.StopButton.TabIndex = 1;
            this.StopButton.Text = "Stop";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // ContinueButton
            // 
            this.ContinueButton.Enabled = false;
            this.ContinueButton.Location = new System.Drawing.Point(174, 12);
            this.ContinueButton.Name = "ContinueButton";
            this.ContinueButton.Size = new System.Drawing.Size(75, 23);
            this.ContinueButton.TabIndex = 2;
            this.ContinueButton.Text = "Continue";
            this.ContinueButton.UseVisualStyleBackColor = true;
            this.ContinueButton.Click += new System.EventHandler(this.ContinueButton_Click);
            // 
            // InputPattern
            // 
            this.InputPattern.Location = new System.Drawing.Point(6, 69);
            this.InputPattern.Name = "InputPattern";
            this.InputPattern.Size = new System.Drawing.Size(408, 20);
            this.InputPattern.TabIndex = 4;
            // 
            // InputTextInFile
            // 
            this.InputTextInFile.Location = new System.Drawing.Point(6, 108);
            this.InputTextInFile.Name = "InputTextInFile";
            this.InputTextInFile.Size = new System.Drawing.Size(408, 20);
            this.InputTextInFile.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Pattern of file name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Text in file:";
            // 
            // InputDirectory
            // 
            this.InputDirectory.Location = new System.Drawing.Point(6, 30);
            this.InputDirectory.Name = "InputDirectory";
            this.InputDirectory.Size = new System.Drawing.Size(408, 20);
            this.InputDirectory.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Directory of search:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Check file now:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Cheking files already:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Time:";
            // 
            // lblCheckNowFile
            // 
            this.lblCheckNowFile.AutoEllipsis = true;
            this.lblCheckNowFile.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblCheckNowFile.Location = new System.Drawing.Point(119, 63);
            this.lblCheckNowFile.Name = "lblCheckNowFile";
            this.lblCheckNowFile.Size = new System.Drawing.Size(295, 54);
            this.lblCheckNowFile.TabIndex = 15;
            this.lblCheckNowFile.Text = "-";
            // 
            // lblcountOfFiles
            // 
            this.lblcountOfFiles.AutoSize = true;
            this.lblcountOfFiles.Location = new System.Drawing.Point(119, 16);
            this.lblcountOfFiles.Name = "lblcountOfFiles";
            this.lblcountOfFiles.Size = new System.Drawing.Size(13, 13);
            this.lblcountOfFiles.TabIndex = 16;
            this.lblcountOfFiles.Text = "0";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(119, 39);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(34, 13);
            this.lblTime.TabIndex = 17;
            this.lblTime.Text = "00:00";
            // 
            // ProcessData
            // 
            this.ProcessData.Controls.Add(this.label4);
            this.ProcessData.Controls.Add(this.lblTime);
            this.ProcessData.Controls.Add(this.label5);
            this.ProcessData.Controls.Add(this.lblcountOfFiles);
            this.ProcessData.Controls.Add(this.label6);
            this.ProcessData.Controls.Add(this.lblCheckNowFile);
            this.ProcessData.Location = new System.Drawing.Point(14, 175);
            this.ProcessData.Name = "ProcessData";
            this.ProcessData.Size = new System.Drawing.Size(420, 127);
            this.ProcessData.TabIndex = 18;
            this.ProcessData.TabStop = false;
            this.ProcessData.Text = "Process data";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(17, 305);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 21;
            // 
            // InputDatas
            // 
            this.InputDatas.Controls.Add(this.InputTextInFile);
            this.InputDatas.Controls.Add(this.InputDirectory);
            this.InputDatas.Controls.Add(this.InputPattern);
            this.InputDatas.Controls.Add(this.label1);
            this.InputDatas.Controls.Add(this.label2);
            this.InputDatas.Controls.Add(this.label3);
            this.InputDatas.Location = new System.Drawing.Point(14, 41);
            this.InputDatas.Name = "InputDatas";
            this.InputDatas.Size = new System.Drawing.Size(420, 134);
            this.InputDatas.TabIndex = 20;
            this.InputDatas.TabStop = false;
            this.InputDatas.Text = "Input data";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "folderImage.png");
            this.imageList1.Images.SetKeyName(1, "fileImage.png");
            // 
            // treeView1
            // 
            this.treeView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeView1.HotTracking = true;
            this.treeView1.ImageIndex = 1;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(440, 12);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.ShowNodeToolTips = true;
            this.treeView1.Size = new System.Drawing.Size(254, 335);
            this.treeView1.TabIndex = 21;
            // 
            // searchFileForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(706, 359);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.InputDatas);
            this.Controls.Add(this.ProcessData);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.FindButton);
            this.Name = "searchFileForm";
            this.Text = "Finding file in derictory";
            this.ProcessData.ResumeLayout(false);
            this.ProcessData.PerformLayout();
            this.InputDatas.ResumeLayout(false);
            this.InputDatas.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button FindButton;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.Button ContinueButton;
        private System.Windows.Forms.TextBox InputPattern;
        private System.Windows.Forms.TextBox InputTextInFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox InputDirectory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblCheckNowFile;
        private System.Windows.Forms.Label lblcountOfFiles;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.GroupBox ProcessData;
        private System.Windows.Forms.GroupBox InputDatas;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TreeView treeView1;
    }
}

