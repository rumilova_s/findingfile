﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace AppFindingFile
{
    public partial class searchFileForm : Form
    {
        private delegate void TreeViewDelegate(TreeNodeCollection nodeList, string path);
        private TreeViewDelegate addTreeNode;
        private DateTime myTimer;
        private string inputDirectory;
        private string inputPattern;
        private string inputText;
        private int countOfFiles;
        static ManualResetEventSlim resetEvent;
        private CancellationTokenSource source;
        private CancellationToken token;
        private bool searchComplete;
        public searchFileForm()
        {
            InitializeComponent();
            ReadInputData();
            myTimer = new DateTime(0, 0);
            countOfFiles = 0;
            addTreeNode = new TreeViewDelegate(AddTreeNode);
            resetEvent = new ManualResetEventSlim(false);
            searchComplete = false;
        }
        //
        //Save input values implementation
        //
        private void ReadInputData()
        {
            InputDirectory.Text = (string)Properties.Settings.Default[InputDirectory.Name];
            InputPattern.Text = (string)Properties.Settings.Default[InputPattern.Name];
            InputTextInFile.Text = (string)Properties.Settings.Default[InputTextInFile.Name];
        }
        private void SaveInputData()
        {
            Properties.Settings.Default[InputDirectory.Name] = InputDirectory.Text;
            Properties.Settings.Default[InputPattern.Name] = InputPattern.Text;
            Properties.Settings.Default[InputTextInFile.Name] = InputTextInFile.Text;

            Properties.Settings.Default.Save();
        }
        void AddTreeNode(TreeNodeCollection nodeList, string path)
        {
            TreeNode node = null;
            string folder = string.Empty;
            bool tag = false;
            int p = path.IndexOf('\\');

            if (p == -1)
            {
                tag = true;
                folder = path;
                path = "";
            }
            else
            {
                folder = path.Substring(0, p);
                path = path.Substring(p + 1, path.Length - (p + 1));
            }
            node = null;
            foreach (TreeNode item in nodeList)
            {
                if (item.Text == folder)
                {
                    node = item;
                }
            }
            if (node == null)
            {
                node = new TreeNode(folder);
                if (tag)
                {
                    node.ImageIndex = 1;
                    node.SelectedImageIndex = 1;
                }
                else
                {
                    node.ImageIndex = 0;
                    node.SelectedImageIndex = 0;
                }
                nodeList.Add(node);
            }
            if (path != "")
            {
                node.Expand();
                AddTreeNode(node.Nodes, path);
            }
            treeView1.Update();
        }
        void AddTextToLbl(Label lbl, string text)
        {
            if (lbl.InvokeRequired)
            {
                try
                {
                    this.Invoke((Action)(() =>
                    {
                        lbl.Text = text;
                    }
                    ));

                }
                catch (ObjectDisposedException)
                {
                    ;
                }
            }
            else
            {
                try
                {
                    lbl.Text = text;
                }
                catch (OutOfMemoryException)
                {
                    ;
                }
            }
        }
        void SearchFiles(string directory, string pattern, string text, CancellationToken t)
        {
            Stack<string> dirs = new Stack<string>();
            Stack<string> result = new Stack<string>();
            dirs.Push(directory);
            while (dirs.Count > 0)
            {
                string currentDirPath = dirs.Pop();
                try
                {
                    string[] subDirs = Directory.GetDirectories(currentDirPath);
                    foreach (string subDirPath in subDirs)
                    {
                        dirs.Push(subDirPath);
                    }
                }
                catch (ArgumentException)
                {
                    AddTextToLbl(lblResult, "Check the path directory!\n");
                    break;
                }
                catch (UnauthorizedAccessException)
                {
                    continue;
                }
                catch (DirectoryNotFoundException)
                {
                    AddTextToLbl(lblResult, "Directory not found.\n");
                    break;
                }
                string[] files = null;
                try
                {
                    files = Directory.GetFiles(currentDirPath, pattern);
                    string[] curFiles = Directory.GetFiles(currentDirPath);
                    countOfFiles += curFiles.Length;
                    AddTextToLbl(lblcountOfFiles, countOfFiles.ToString());
                }
                catch (UnauthorizedAccessException)
                {
                    continue;
                }
                try
                {
                    resetEvent.Wait(t);
                }
                catch (OperationCanceledException)
                {
                    if (t.IsCancellationRequested)
                    {
                        Invoke((Action)(() => treeView1.Nodes.Clear()));
                        result.Clear();
                        return;
                    }
                }
                Parallel.ForEach(files, (filePath, state) =>
                {
                    AddTextToLbl(lblCheckNowFile, filePath.ToString());
                    if (SearchText(filePath, text))
                    {
                        result.Push(filePath);
                        try
                        {
                            Invoke(addTreeNode, new object[] { treeView1.Nodes, filePath });
                        }
                        catch (ObjectDisposedException)
                        {
                            ;
                        }
                    }
                });
            }
            if (result.Count != 0)
            {
                result.Clear();
            }
            else AddTextToLbl(lblResult, lblResult.Text + "No file with this criteries.\n");
            searchComplete = true;
        }
        bool SearchText(string fileName, string searchText)
        {
            try
            {
                string fileCheck = File.ReadAllText(fileName);

                if (fileCheck.Contains(searchText))
                {
                    return true;
                }
            }
            catch (Exception)
            {
                ;
            }
            return false;
        }
        private async void FindButton_Click(object sender, EventArgs e)
        {
            myTimer = new DateTime(0, 0);
            if (!timer.Enabled)
                timer.Enabled = true;
            SaveInputData();
            treeView1.Nodes.Clear();
            countOfFiles = 0;
            lblcountOfFiles.Text = "0";
            lblResult.Text = "";
            lblCheckNowFile.Text = "-";

            FindButton.Enabled = false;
            StopButton.Enabled = true;
            ContinueButton.Enabled = false;

            InputDirectory.Refresh();
            InputPattern.Refresh();
            InputTextInFile.Refresh();

            inputDirectory = InputDirectory.Text;
            inputPattern = InputPattern.Text;
            inputText = InputTextInFile.Text;

            if (source != null)
            {
                source.Cancel();
                source.Dispose();
            }
            searchComplete = false;
            source = new CancellationTokenSource();
            token = source.Token;
            resetEvent.Set();
            await Task.Run(() => SearchFiles(inputDirectory, inputPattern, inputText, token), token);
            if (searchComplete)
            {
                if (timer.Enabled)
                    timer.Enabled = false;
                StopButton.Enabled = false;
                FindButton.Enabled = true;
                lblCheckNowFile.Text = "-";
                lblResult.Text += "Done.\n";
            }
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (!timer.Enabled)
                timer.Enabled = true;
            myTimer = myTimer.AddSeconds(1);
            lblTime.Text = myTimer.ToString("mm:ss");
        }
        private void StopButton_Click(object sender, EventArgs e)
        {
            resetEvent.Reset();
            
            FindButton.Enabled = true;
            StopButton.Enabled = false;
            ContinueButton.Enabled = true;
            if (timer.Enabled)
                timer.Enabled = false;
        }
        private void ContinueButton_Click(object sender, EventArgs e)
        {
            resetEvent.Set();
            StopButton.Enabled = true;
            ContinueButton.Enabled = false;
            FindButton.Enabled = false;
            if (!timer.Enabled)
                timer.Enabled = true;
        }
    }
}
